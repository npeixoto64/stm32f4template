#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"
#include "stm32f4xx_hal.h"

// ----------------------------------------------------------------------------
//
// Standalone STM32F4 empty sample (trace via ITM).
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the ITM output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

/*-----------------------------------------------------------*/

caddr_t _sbrk(int incr);

caddr_t _sbrk(int incr)
{
  return (caddr_t) NULL;
}

void __initialize_hardware(void);

int main(int argc, char* argv[])
{
	__initialize_hardware();

	GPIO_InitTypeDef ld4_init = { 	GPIO_PIN_12,
									GPIO_MODE_OUTPUT_PP,
									GPIO_NOPULL,
									GPIO_SPEED_LOW,
									0
	};

	GPIO_InitTypeDef ld5_init = { 	GPIO_PIN_14,
									GPIO_MODE_OUTPUT_PP,
									GPIO_NOPULL,
									GPIO_SPEED_LOW,
									0
	};

	HAL_GPIO_Init(GPIOD, &ld4_init);
	HAL_GPIO_Init(GPIOD, &ld5_init);

	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);

	while(1);
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
